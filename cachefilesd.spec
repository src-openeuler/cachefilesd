Name:           cachefilesd
Version:        0.10.10
Release:        11
Summary:        CacheFiles user-space management daemon
Group:          System Environment/Daemons
License:        GPLv2+
URL:            http://people.redhat.com/~dhowells/fscache/
Source0:        https://people.redhat.com/dhowells/cachefs/cachefilesd-%{version}.tar.bz2

Patch1: 0001-cachefilesd-fix-abort-in-decant_cull_table.patch

BuildRequires:  gcc systemd-units 
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: selinux-policy-base >= 3.7.19-5

%description
The cachefilesd daemon manages the caching files and directory that are that
are used by network file systems such a AFS and NFS to do persistent caching to
the local disk.

%package  help
Summary:Files for cachefilesd help

%description help
Files for cachefilesd help

%prep
%autosetup -n %{name}-%{version} -p1

%build
make all CFLAGS="-Wall -Werror $RPM_OPT_FLAGS $RPM_LD_FLAGS $ARCH_OPT_FLAGS"

%install
mkdir -p %{buildroot}%{_mandir}/{man5,man8}
mkdir -p %{buildroot}%{_localstatedir}/cache/fscache
make DESTDIR=%{buildroot} install \
        ETCDIR=%{_sysconfdir} \
        SBINDIR=%{_sbindir}\
        MANDIR=%{_mandir}\
        CFLAGS="-Wall $RPM_OPT_FLAGS -Werror"

install -m    644 cachefilesd.conf %{buildroot}%{_sysconfdir}
install -D -m 644 cachefilesd.service %{buildroot}%{_unitdir}/cachefilesd.service

%post
%systemd_post cachefilesd.service

%preun
%systemd_preun cachefilesd.service

%postun
%systemd_postun_with_restart cachefilesd.service

%files
%doc README selinux/*
%config(noreplace) %{_sysconfdir}/cachefilesd.conf
%{_sbindir}/*
%{_unitdir}/*
%{_localstatedir}/cache/fscache

%files help
%{_mandir}/*/*

%changelog
* Tue Jul 30 2024 zhangjian <zhangjian496@huawei.com> - 0.10.10-11
- Type:cleancode
- ID:NA
- SUG:restart
- DESC:remove space on the end of spec

* Fri Nov 4 2022 zhanchengbin <zhanchengbin1@huawei.com> - 0.10.10-10
- DESC: synchronize version

* Mon Nov 29 2021 volcanodragon <linfeilong@huawei.com> - 0.10.10-9
- DESC: fix decant_cull_table to support 64 bit.

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.10.10-8
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Sep 8 2020 lihaotian<lihaotian9@huawei.com> - 0.10.10-7
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:Update the source0 url

* Fri Sep 6 2019 sunshihao<sunshihao@huawei.com> - 0.10.10-6
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC:openEuler Debranding.
- Package Init

